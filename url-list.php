<?php

require_once ('DatabaseConnect.php');
require_once ('Url.php');

$urlList = Url::getUrlListFromDb();

?>

<!DOCTYPE html>
<html>
<head>
    <title>URL list</title>
    <meta charset="utf-8">
    <meta name="description" content="Stas Chyrkov Test">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div id="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Main<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="url-list.php">URL list<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div id="content">
        <h1>URL list</h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">№</th>
                <th scope="col">Original URL</th>
                <th scope="col">Short URL</th>
                <th scope="col">Created</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($urlList as $url) : ?>
            <tr>
                <th scope="row"><?= $url->getId() ?></th>
                <td><?= $url->getOriginalUrl() ?></td>
                <td><?= $url->getShortUrl() ?></td>
                <td><?= $url->getCreated() ?></td>
            </tr>
            <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
