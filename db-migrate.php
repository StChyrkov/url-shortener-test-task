<?php

require_once ('DatabaseConnect.php');

$pdo = new DatabaseConnect();

try {
    $sql = '
		create table urls (
			`id` int not null auto_increment primary key,
			`originalUrl` varchar(100), 
			`shortUrl` varchar(100), 
			`created` timestamp default current_timestamp not null
		);
	';

    $pdo->connection->exec($sql);

    $sql = 'insert into urls set
        `originalUrl` =:originalUrl,  
		`shortUrl`=:shortUrl
		';

    $query = $pdo->connection->prepare($sql);

    $query->bindValue(':originalUrl', 'http://example.com/category/');
    $query->bindValue(':shortUrl', 'http://url-shortener.local/abCdE');

    $query->execute();

    echo 'Migration successful';

} catch(Exception $error) {
    $error = 'Migration failed';
    echo $error;
}