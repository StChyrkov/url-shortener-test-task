<?php


class Url
{
    protected $id;
    protected $originalUrl;
    protected $shortUrl;
    protected $created;

    public function __construct($id, $originalUrl, $shortUrl, $created)
    {
        $this->id = $id;
        $this->originalUrl = $originalUrl;
        $this->shortUrl = $shortUrl;
        $this->created = $created;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOriginalUrl()
    {
        return $this->originalUrl;
    }

    public function getShortUrl()
    {
        return $this->shortUrl;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public static function getUrlListFromDb()
    {
        $db = new DatabaseConnect();

        $sql = 'select * from `urls`';

        $query = $db->connection->prepare($sql);

        $query->execute();

        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        $urlList = [];

        foreach ($data as $url) {
            $urlList[] = new Url($url['id'], $url['originalUrl'], $url['shortUrl'], $url['created']);
        }
        return $urlList;
    }

    public static function matchOriginalUrl($originalUrl)
    {

        $db = new DatabaseConnect();

        $sql = 'select exists (select * from `urls` where `originalUrl`=:originalUrl)';

        $query = $db->connection->prepare($sql);

        $query->bindValue('originalUrl', $originalUrl);

        $query->execute();

        $data = $query->fetch();

        $match = $data[0];

        return $match;
    }

    public static function matchShortUrl($shortUrl)
    {

        $db = new DatabaseConnect();

        $sql = 'select exists (select * from `urls` where `shortUrl`=:shortUrl)';

        $query = $db->connection->prepare($sql);

        $query->bindValue('shortUrl', $shortUrl);

        $query->execute();

        $data = $query->fetch();

        $match = $data[0];

        return $match;
    }

    public static function getUrlFromDbByOriginal($originalUrl)
    {
        $db = new DatabaseConnect();

        $sql = 'select * from urls where `originalUrl`=:originalUrl';

        $query = $db->connection->prepare($sql);

        $query->bindValue('originalUrl', $originalUrl);

        $query->execute();

        $data = $query->fetchObject();

        return new Url(
            $data->id,
            $data->originalUrl,
            $data->shortUrl,
            $data->created
        );
    }

    public static function getUrlFromDbByShort($shortUrl)
    {
        $db = new DatabaseConnect();

        $sql = 'select * from urls where `shortUrl`=:shortUrl';

        $query = $db->connection->prepare($sql);

        $query->bindValue('shortUrl', $shortUrl);

        $query->execute();

        $data = $query->fetchObject();

        return new Url(
            $data->id,
            $data->originalUrl,
            $data->shortUrl,
            $data->created
        );
    }

    public static function createShortUrl($originalUrl)
    {
        $random = 'qQwWeErRtTyYuUiIoOpPaAsSdDfFgGhHjJkKlLzZxXcCvVbBnNmM1234567890';

        $shortUrl = 'http://url-shortener.local/' . substr(str_shuffle($random), 0, 5);

        $match = Url::matchShortUrl($shortUrl);

        if ($match == 0){
            $db = new DatabaseConnect();

            $sql = 'insert into urls set
                    `originalUrl` =:originalUrl,  
		            `shortUrl`=:shortUrl
		            ';

            $query = $db->connection->prepare($sql);

            $query->bindValue(':originalUrl', $originalUrl);
            $query->bindValue(':shortUrl', $shortUrl);

            $query->execute();

            $url = Url::getUrlFromDbByShort($shortUrl);

            return $url;
        } else {
            Url::createShortUrl($originalUrl);
        }
    }
}