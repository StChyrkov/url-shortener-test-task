<?php

require_once ('DatabaseConnect.php');
require_once ('Url.php');

if (isset($_POST['originalUrl'])){
    $originalUrl = htmlspecialchars($_POST['originalUrl']);
} else {
    header('Location: index.php');
}

$match = Url::matchOriginalUrl($originalUrl);

if ($match == 1){
    $url = Url::getUrlFromDbByOriginal($originalUrl);
} else {
    $url = Url::createShortUrl($originalUrl);
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>URL shortener</title>
    <meta charset="utf-8">
    <meta name="description" content="Stas Chyrkov Test">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div id="content">
        <h1>Short URL</h1>
        <?=$url->getShortUrl() ?>
        <p><a class="btn btn-primary" href="index.php">Back to main page</a></p>
    </div>
</div>
</body>
</html>
