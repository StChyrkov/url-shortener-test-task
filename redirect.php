<?php

require_once ('DatabaseConnect.php');
require_once ('Url.php');

if (isset($_GET['url'])){
    $shortUrl = htmlspecialchars($_GET['url']);
} else {
    header ('Location: index.php');
}

$match = Url::matchShortUrl($shortUrl);

if ($match == 1){
    $url = Url::getUrlFromDbByShort($shortUrl);
    header ('Location: ' . $url->getOriginalUrl());
} else {
    $urlError = 'Short URL does not exist';
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>URL shortener</title>
    <meta charset="utf-8">
    <meta name="description" content="Stas Chyrkov Test">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div id="content">
        <?php if(isset ($urlError)) : ?>
            <h1><?=$urlError ?></h1>
            <p><a class="btn btn-primary" href="index.php">Back to main page</a></p>
        <?php endif ?>
    </div>
</div>
</body>
</html>