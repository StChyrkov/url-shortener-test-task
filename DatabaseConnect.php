<?php


class DatabaseConnect
{
    public $connection;

    public function __construct()
    {
        try {
            $this->connection = new PDO(
                'mysql:host=localhost;dbname=urldb',
                'urldb_user1',
                '123456'
            );

            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            $error = 'Ошибка подключения к базе данных';
            echo $error;
        }
    }
}